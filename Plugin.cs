﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace example
{
    class Plugin
    {
        const string PLUGIN_NAME = "ABBY"; // should be same as namespace of the node.
        static void Main(string[] args)
        {
            IEnumerable<System.Type> classes = AppDomain.CurrentDomain.GetAssemblies()
                       .SelectMany(t => t.GetTypes())
                       .Where(t => t.IsClass && t.Namespace == PLUGIN_NAME && t.IsPublic && t.GetConstructors().Length > 0);

            foreach (Type c in classes)
            {
                Type ty = Type.GetType(c.FullName);
                Activator.CreateInstance(ty);
            }
			
            try {
                Mosteknoloji.Robomotion.Plugin.Run();
            } catch (Exception ex) {
                foreach(Mosteknoloji.Robomotion.NodeFactory nf in  Mosteknoloji.Robomotion.Plugin.NFactories)
                {
                    string guid = nf.Node.props != null ? nf.Node.props.Guid : "";
                    Mosteknoloji.Robomotion.Log.Error(String.Format("{0}.Plugin run failed: {1}", guid, ex.Message));
                }
            }
            
        }
    }
}

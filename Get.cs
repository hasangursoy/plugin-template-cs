﻿using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;
using Mosteknoloji.Robomotion;

namespace Clipboard
{
    public class Get : Node
    {
        private static string NodeName = "Plugin.Clipboard.Get";
        private struct NodeProps : INodeProps
        {
            public string Guid { get; set; }
            public string Name { get; set; }
            public float DelayBefore { get; set; }
            public float DelayAfter { get; set; }
            public Variable Variable { get; set; }
        }

        public Get()
        {
            this.OnCreateHandler = new Node.OnCreateCallback(this.OnCreate);
            this.OnMessageHandler = new Node.OnMessageCallback(this.OnMessage);
            this.OnCloseHandler = new Node.OnCloseCallback(this.OnDestroy);
            this.props = new NodeProps();
            NodeFactory nf = new NodeFactory(this, NodeName);
        }

        private void OnCreate(Node _node, byte[] config)
        {

        }

        private byte[] OnMessage(Node _node, byte[] data)
        {
            Dictionary<string, object> msg = new Dictionary<string, object>();
            string encData = Encoding.UTF8.GetString(data);
            msg = JsonConvert.DeserializeObject<Dictionary<string, object>>(encData);
            msg["payload"] = "cs plugin";
            return Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(msg));
        }

        private void OnDestroy()
        {

        }
    }
}
